package com.android.audiovideoplayer.model

import android.graphics.Bitmap
import android.net.Uri

data class MediaFile (
    val id: Long,
    val title: String?,
    val albumId: Long?,
    val album: String?,
    val artistId: Long?,
    val artist: String?,
    val path: String?,
    val size: String?,
    val duration: String?,
    val mimeType: String?,
    val uri: Uri?,
    val coverImage: Bitmap?
)