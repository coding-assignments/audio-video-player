package com.android.audiovideoplayer.util

import com.android.audiovideoplayer.BuildConfig

object PlayerConstants {

    const val PACKAGE_NAME = BuildConfig.APPLICATION_ID
}