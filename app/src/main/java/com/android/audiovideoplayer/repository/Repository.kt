package com.android.audiovideoplayer.repository

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import com.android.audiovideoplayer.model.MediaFile
import com.android.audiovideoplayer.vo.Resource
import com.android.audiovideoplayer.vo.Status
import javax.inject.Inject
import javax.inject.Singleton
import com.android.audiovideoplayer.util.FileUtil
import com.android.audiovideoplayer.util.FileUtil.getReadableFileDuration
import com.android.audiovideoplayer.util.FileUtil.getReadableFileSize
import java.io.File
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.content.ContentUris

@Singleton
class Repository @Inject constructor(private val contentResolver: ContentResolver) {

    suspend fun loadAudioFiles(): Resource<MutableList<MediaFile>> {
        // Initialize an empty mutable list of audio
        val list: MutableList<MediaFile> = mutableListOf()

        // Get the external storage media store audio uri
        val uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI

        // IS_MUSIC : Non-zero if the audio file is audio
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0"

        // Sort the musics
        val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"

        // Query the external storage for audio files
        val cursor: Cursor? = contentResolver.query(
            uri, // Uri
            null, // Projection
            selection, // Selection
            null, // Selection arguments
            sortOrder // Sort order
        )

        cursor?.let {

            // If query result is not empty
            if (cursor.moveToFirst()) {
                val id: Int = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
                val title: Int = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
                val albumId: Int = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)
                val album: Int = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)
                val artistId: Int = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID)
                val artist: Int = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
                val size: Int = cursor.getColumnIndex(MediaStore.Audio.Media.SIZE)
                val path: Int = cursor.getColumnIndex(MediaStore.Audio.Media.DATA)
                val duration: Int = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)
                val mimeType: Int = cursor.getColumnIndex(MediaStore.Audio.Media.MIME_TYPE)

                // Now loop through the audio files
                do {
                    val audioId: Long = cursor.getLong(id)
                    val audioTitle: String? = cursor.getString(title)
                    val audioAlbumID: Long = cursor.getLong(albumId)
                    val audioAlbum: String? = cursor.getString(album)
                    val audioArtistID: Long = cursor.getLong(artistId)
                    val audioArtist: String? = cursor.getString(artist)
                    val audioSize: String? = cursor.getString(size)
                    val audioPath: String? = cursor.getString(path)
                    val audioDuration: String? = cursor.getString(duration)
                    val audioMimeType: String? = cursor.getString(mimeType)

                    val uri = Uri.parse(audioPath)
                    val file = File(uri.path!!)

                    // Add the current audio to the list
                    list.add(
                        MediaFile(
                            audioId,
                            audioTitle,
                            audioAlbumID,
                            audioAlbum,
                            audioArtistID,
                            audioArtist,
                            audioPath,
                            getReadableFileSize(audioSize?.toInt()!!),
                            getReadableFileDuration(audioDuration?.toLong()!!),
                            audioMimeType,
                            FileUtil.getUri(File(file.path)),
                            getCover(audioAlbumID)
                        )
                    )
                } while (cursor.moveToNext())
            }

        }

        cursor?.close()

        // Finally, return the audio files list
        return Resource(Status.SUCCESS, list, null)
    }

    suspend fun loadVideoFiles(): Resource<MutableList<MediaFile>> {
        // Initialize an empty mutable list of video
        val list: MutableList<MediaFile> = mutableListOf()

        // Get the external storage media store video uri
        val uri: Uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI

        // Sort the videos
        val sortOrder = MediaStore.Video.Media.TITLE + " ASC"

        // Query the external storage for video files
        val cursor: Cursor? = contentResolver.query(
            uri, // Uri
            null, // Projection
            null, // Selection
            null, // Selection arguments
            sortOrder // Sort order
        )

        cursor?.let {

            // If query result is not empty
            if (cursor.moveToFirst()) {
                val id: Int = cursor.getColumnIndex(MediaStore.Video.Media._ID)
                val title: Int = cursor.getColumnIndex(MediaStore.Video.Media.TITLE)
                val album: Int = cursor.getColumnIndex(MediaStore.Video.Media.ALBUM)
                val artist: Int = cursor.getColumnIndex(MediaStore.Video.Media.ARTIST)
                val size: Int = cursor.getColumnIndex(MediaStore.Video.Media.SIZE)
                val path: Int = cursor.getColumnIndex(MediaStore.Video.Media.DATA)
                val duration: Int = cursor.getColumnIndex(MediaStore.Video.Media.DURATION)
                val mimeType: Int = cursor.getColumnIndex(MediaStore.Video.Media.MIME_TYPE)

                // Now loop through the video files
                do {
                    val videoId: Long = cursor.getLong(id)
                    val videoTitle: String? = cursor.getString(title)
                    val videoAlbum: String? = cursor.getString(album)
                    val videoArtist: String? = cursor.getString(artist)
                    val videoSize: String? = cursor.getString(size)
                    val videoPath: String? = cursor.getString(path)
                    val videoDuration: String? = cursor.getString(duration)
                    val videoMimeType: String? = cursor.getString(mimeType)

                    // Add the current video to the list
                    list.add(
                        MediaFile(
                            videoId,
                            videoTitle,
                            0L,
                            videoAlbum,
                            0L,
                            videoArtist,
                            videoPath,
                            getReadableFileSize(videoSize?.toInt()!!),
                            getReadableFileDuration(videoDuration?.toLong()!!),
                            videoMimeType,
                            FileUtil.getUri(File(videoPath)),
                            getCoverFromFile(FileUtil.getUri(File(videoPath))?.path)
                        )
                    )
                } while (cursor.moveToNext())

            }

            cursor.close()

        }// Finally, return the video files list
        return Resource(Status.SUCCESS, list, null)
    }

    fun getCoverFromFile(path: String?): Bitmap? {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        val bitmap = BitmapFactory.decodeFile(path, options)
        return bitmap
    }


    fun getCover(albumId: Long): Bitmap? {
        val sArtworkUri = Uri.parse("content://media/external/audio/albumart")
        val uri = ContentUris.withAppendedId(sArtworkUri, albumId)
        try {
            val input = contentResolver.openInputStream(uri)
            return BitmapFactory.decodeStream(input)
        } catch (e: java.io.FileNotFoundException) {
            e.printStackTrace()
        }
        return null
    }

}