package com.android.audiovideoplayer.di.module

import com.android.audiovideoplayer.ui.fragment.audio.AudioFragment
import com.android.audiovideoplayer.ui.fragment.video.VideoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class MediaActivityFragmentBuilder {

    @ContributesAndroidInjector
    abstract fun contributeAudioFragment(): AudioFragment

    @ContributesAndroidInjector
    abstract fun contributeVideoFragment(): VideoFragment
}