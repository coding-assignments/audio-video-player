package com.android.audiovideoplayer.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.audiovideoplayer.di.ViewModelKey
import com.android.audiovideoplayer.ui.fragment.audio.AudioViewModel
import com.android.audiovideoplayer.ui.fragment.video.VideoViewModel
import com.android.audiovideoplayer.vm.PlayerViewModelFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AudioViewModel::class)
    abstract fun bindAudioViewModel(audioViewModel: AudioViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VideoViewModel::class)
    abstract fun bindVideoViewModel(videoViewModel: VideoViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: PlayerViewModelFactory): ViewModelProvider.Factory
}
