package com.android.audiovideoplayer.di.module

import com.android.audiovideoplayer.ui.MediaActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MediaActivityFragmentBuilder::class])
    abstract fun contributeMediaActivity(): MediaActivity
}