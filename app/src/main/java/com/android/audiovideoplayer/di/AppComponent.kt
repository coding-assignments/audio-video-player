package com.android.audiovideoplayer.di

import android.app.Application
import com.android.audiovideoplayer.PlayerApplication
import com.android.audiovideoplayer.di.module.ActivityBindingModule
import com.android.audiovideoplayer.di.module.AppModule
import com.android.audiovideoplayer.di.module.NetworkModule
import com.android.audiovideoplayer.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        ActivityBindingModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(playerApplication: PlayerApplication)
}
