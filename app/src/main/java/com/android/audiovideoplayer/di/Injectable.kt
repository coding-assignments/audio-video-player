package com.android.audiovideoplayer.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
