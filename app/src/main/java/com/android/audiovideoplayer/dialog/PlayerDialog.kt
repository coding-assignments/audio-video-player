package com.android.audiovideoplayer.dialog

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.android.audiovideoplayer.R
import com.android.audiovideoplayer.databinding.DialogPlayerBinding
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.android.synthetic.main.dialog_player.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory


class PlayerDialogHelper(
    val uri: Uri?,
    val cancelable: Boolean): BaseDialogHelper(PlayerDialogFragment())

class PlayerDialogFragment: BaseDialogFragment<PlayerDialogHelper>(){

    private lateinit var binding: DialogPlayerBinding

    private var player: SimpleExoPlayer? = null

    var playWhenReady = true
    var currentWindow = 0
    var playbackPosition = 0L

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_player, container, false)

        isCancelable = helper.cancelable
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width, height)
        }

        initializePlayer()
    }

    override fun onStop() {
        super.onStop()

        releasePlayer()
    }

    private fun initializePlayer() {
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(activity,
                DefaultRenderersFactory(context),
                DefaultTrackSelector(),
                DefaultLoadControl())

            video_view?.player = player

            player?.playWhenReady = playWhenReady
            player?.seekTo(currentWindow, playbackPosition)
        }
        val mediaSource = helper.uri?.let { buildMediaSource(it) }
        player?.prepare(mediaSource, true, false)
    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        return ExtractorMediaSource(
            uri,
            DefaultDataSourceFactory(activity, "ua"),
            DefaultExtractorsFactory(), null, null
        )
    }

    private fun releasePlayer() {
        player?.let {
            playbackPosition = player!!.currentPosition
            currentWindow = player!!.currentWindowIndex
            playWhenReady = player!!.playWhenReady
            player!!.release()
            player = null
        }
    }

}