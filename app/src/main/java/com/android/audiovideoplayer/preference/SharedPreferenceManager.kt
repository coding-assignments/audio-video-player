package com.android.audiovideoplayer.preference

import android.content.SharedPreferences
import com.android.audiovideoplayer.util.PlayerConstants.PACKAGE_NAME

class SharedPreferenceManager(private val sharedPreferences: SharedPreferences) {

    companion object Keys {
        const val SHARED_PREFERENCE_NAME = "$PACKAGE_NAME.SC_SHARED_PREFERENCES"
    }
}