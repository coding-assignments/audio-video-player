package com.android.audiovideoplayer.binding

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.android.audiovideoplayer.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import javax.inject.Inject

/**
 * Binding adapters that work with a fragment instance.
 */
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    
    @BindingAdapter(value = ["imageUrl", "imageRequestListener"], requireAll = false)
    fun bindImage(imageView: ImageView, uri: Uri?, listener: RequestListener<Drawable?>?) {
        Glide.with(fragment).load(uri).apply(RequestOptions().override(500, 500)).listener(listener).into(imageView)
    }

    @BindingAdapter(value = ["bitmap", "imageRequestListener"], requireAll = false)
    fun bindImage(imageView: ImageView, bitmap: Bitmap?, listener: RequestListener<Drawable?>?) {
        Glide.with(fragment).load(bitmap).apply(RequestOptions().override(500, 500)).listener(listener).into(imageView)
    }
}

