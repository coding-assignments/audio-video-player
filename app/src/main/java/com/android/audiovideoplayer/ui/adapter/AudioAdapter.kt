package com.android.audiovideoplayer.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.android.audiovideoplayer.R
import com.android.audiovideoplayer.adapter.DataBoundListAdapter
import com.android.audiovideoplayer.databinding.ItemAudioBinding
import com.android.audiovideoplayer.model.MediaFile
import com.android.audiovideoplayer.util.AppExecutors

/**
 * A RecyclerView adapter for [MediaFile] class.
 */
class AudioAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val clickCallback: ((musicFile: MediaFile) -> Unit)?
) : DataBoundListAdapter<MediaFile, ItemAudioBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<MediaFile>() {
        override fun areItemsTheSame(oldItem: MediaFile, newItem: MediaFile): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MediaFile, newItem: MediaFile): Boolean {
            return oldItem.artist == newItem.artist
                    && oldItem.album == newItem.album
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemAudioBinding {
        val binding = DataBindingUtil.inflate<ItemAudioBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_audio,
            parent,
            false,
            dataBindingComponent
        )
        binding.root.setOnClickListener {
            binding.mediaFile.let {
                it?.let { it1 -> clickCallback?.invoke(it1) }
            }
        }
        return binding
    }

    override fun bind(binding: ItemAudioBinding, item: MediaFile, position: Int) {
        binding.mediaFile = item

    }
}
