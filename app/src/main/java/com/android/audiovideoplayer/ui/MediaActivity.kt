package com.android.audiovideoplayer.ui

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.android.audiovideoplayer.R
import com.android.audiovideoplayer.databinding.ActivityMediaBinding
import com.android.audiovideoplayer.util.permissionHandler.ActivityPermissionHandler
import com.android.audiovideoplayer.util.permissionHandler.BasePermissionHandler
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_media.*
import javax.inject.Inject

class MediaActivity : AppCompatActivity(), HasSupportFragmentInjector, BasePermissionHandler.RequestPermissionListener  {

    private lateinit var binding: ActivityMediaBinding

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private val permissionHandler: ActivityPermissionHandler by lazy {
        ActivityPermissionHandler(
            this,
            permissions = criticalPermissions,
            listener = this)
    }

    private var criticalPermissions: MutableSet<String> = mutableSetOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permissionHandler.requestPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHandler.onRequestPermissionsResult(requestCode, permissions,
            grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        permissionHandler.onActivityResult(requestCode)
    }

    override fun onPermissionsGranted(grantedPermissions: Set<String>) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_media)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_audio, R.id.navigation_video
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)
    }

    override fun onPermissionsNotGranted(deniedPermissions: Set<String>) {
        permissionHandler.requestPermission()
    }

    override fun onShowPermissionRationale(permissions: Set<String>): Boolean {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.permissions_rationale))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                permissionHandler.retryRequestDeniedPermission()
            }
            .show()
        return true // if don't want to show any rationale, just return false here
    }

    override fun onShowSettingRationale(permissions: Set<String>): Boolean {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.permissions_settings_rationale))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.settings)) { _, _ ->
                permissionHandler.requestPermissionInSetting()
            }
            .show()
        return true // if don't want to show any rationale, just return false here
    }
}
