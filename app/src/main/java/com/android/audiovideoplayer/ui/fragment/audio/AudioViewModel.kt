package com.android.audiovideoplayer.ui.fragment.audio

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.android.audiovideoplayer.model.MediaFile
import com.android.audiovideoplayer.repository.Repository
import com.android.audiovideoplayer.vo.Resource
import javax.inject.Inject

class AudioViewModel@Inject constructor(
    private val repository: Repository
) : ViewModel() {

    val audioFiles: LiveData<Resource<MutableList<MediaFile>>> = liveData {
        val data = repository.loadAudioFiles()
        emit(data)
    }
}