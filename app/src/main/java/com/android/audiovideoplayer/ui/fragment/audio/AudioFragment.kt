package com.android.audiovideoplayer.ui.fragment.audio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.android.audiovideoplayer.ui.adapter.AudioAdapter
import com.android.audiovideoplayer.R
import com.android.audiovideoplayer.binding.FragmentDataBindingComponent
import com.android.audiovideoplayer.databinding.FragmentAudioBinding
import com.android.audiovideoplayer.di.Injectable
import com.android.audiovideoplayer.dialog.PlayerDialogFragment
import com.android.audiovideoplayer.dialog.PlayerDialogHelper
import com.android.audiovideoplayer.util.AppExecutors
import com.android.audiovideoplayer.util.autoCleared
import javax.inject.Inject

class AudioFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val audioViewModel: AudioViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AudioViewModel::class.java)
    }

    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<FragmentAudioBinding>()
    var mediaAdapter by autoCleared<AudioAdapter>()

    private lateinit var playerDialogHelper: PlayerDialogHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_audio, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.lifecycleOwner = viewLifecycleOwner

        initRecyclerView()

        mediaAdapter = AudioAdapter(
            dataBindingComponent = dataBindingComponent,
            appExecutors = appExecutors, clickCallback = {
                playerDialogHelper = PlayerDialogHelper(it.uri, true)
                playerDialogHelper.attachTo(context as FragmentActivity)
            })

        binding.mediaList.adapter = mediaAdapter
    }

    private fun initRecyclerView() {
        audioViewModel.audioFiles.observe(viewLifecycleOwner, Observer { result ->
            mediaAdapter.submitList(result.data)
        })
    }
}