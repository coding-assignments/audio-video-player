package com.android.audiovideoplayer.ui.fragment.video

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.android.audiovideoplayer.ui.adapter.VideoAdapter
import com.android.audiovideoplayer.R
import com.android.audiovideoplayer.binding.FragmentDataBindingComponent
import com.android.audiovideoplayer.databinding.FragmentVideoBinding
import com.android.audiovideoplayer.di.Injectable
import com.android.audiovideoplayer.dialog.PlayerDialogHelper
import com.android.audiovideoplayer.util.AppExecutors
import com.android.audiovideoplayer.util.autoCleared
import javax.inject.Inject

class VideoFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val videoViewModel: VideoViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VideoViewModel::class.java)
    }

    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<FragmentVideoBinding>()
    var mediaAdapter by autoCleared<VideoAdapter>()

    private lateinit var playerDialogHelper: PlayerDialogHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.lifecycleOwner = viewLifecycleOwner

        initRecyclerView()

        mediaAdapter = VideoAdapter(
            dataBindingComponent = dataBindingComponent,
            appExecutors = appExecutors, clickCallback = {
                playerDialogHelper = PlayerDialogHelper(it.uri, true)
                playerDialogHelper.attachTo(context as FragmentActivity)
            })

        binding.mediaList.adapter = mediaAdapter
    }

    private fun initRecyclerView() {
        videoViewModel.videoFiles.observe(viewLifecycleOwner, Observer { result ->
            mediaAdapter.submitList(result.data)
        })
    }
}